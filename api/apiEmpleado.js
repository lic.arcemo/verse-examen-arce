var empleado = require('../models/empleado'); //Modelo Cliente y métodos getClientes, postCliente
var apiEmpleado = {};

apiEmpleado.get = function(router){
  router.get('/empleados', function(request, response) {
    empleado.getEmpleados(function(error, data){
      console.log(data);
      response.status(200).json(data);
    });
  });
  router.post('/empleados', function(req,res){
    console.log(req.body);
      empleado.new(req.body,res);
    //sucursal.save();
  });
  router.delete('/empleados', function(req,res){
    console.log(req.body);
      empleado.delete(req.body,res);
    //sucursal.save();
  });
  router.put('/empleados', function(req,res){
    console.log(req.body);
      empleado.update(req.body,res);
    //sucursal.save();
  });

}

module.exports = apiEmpleado;
