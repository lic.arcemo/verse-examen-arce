var app = angular.module("Verse",["ServicesVerse"]);

app.controller("start", function($http,$scope, Api){
  console.log("starting");
  $scope.empleados = null;
  Api.getData('/empleados',{}).then(function(data){ //Llmams a el service Api, método getData para obtener los datos.
    $scope.empleados = data.data;
    console.log($scope.empleados);
  });
  $scope.status = [{'id': 1, 'label': 'Si'},{'id': 0, 'label': 'No'}]; // Los tipos de status 1 emp= activa y 0 inactiva
  $scope.types = ["update","insert","delete"]; //Vamos a utilizar estos tipos para identificar si será update o insert en el mismo formulario (Modal)

  $scope.emp={};
  $scope.message={};

  $scope.abrirModal = function(tipo, empleado){
    $scope.message={};
    if(tipo=="update"){
      $scope.type=$scope.types[0]; //Actualizamos el valor del guardado a update
      $scope.emp=empleado; //Suc toma el valor de la empleado seleccionada
    }
    else if(tipo=="insert") {
      $scope.type=$scope.types[1];
      $scope.emp={};
    }
    else{
      $scope.type=$scope.types[2];
      $scope.emp=empleado;
      $scope.save();
    }
  };
  $scope.save = function(){
    if($scope.type=='update'){
      Api.updateData('/empleados',$scope.emp).then( function(data){ //UpdateData de ServicesVerse
        if(data.status==200){
          $scope.message.text ="Actualizado correctamente!";
          $scope.message.type="text-success";
        }
      });
    }
    else if($scope.type=="insert"){
      var today = new Date();
      var today1 = new Date(today.getMonth()+ 1 +"/"+ today.getDate() + "/" + today.getFullYear());
      $scope.emp.CreatedDate = today1;
      console.log($scope.emp.CreatedDate);
      Api.insertData('/empleados',$scope.emp).then( function(data){
        if(data.status==200){
          $scope.message.text ="Guardado correctamente!";
          $scope.message.type="text-success";
          $scope.empleados.push(data.data);
        }
      });
    }
    else{
      console.info("eliminar");
      Api.deleteData('/empleados',$scope.emp).then( function(data){
        if(data.status==200){
          $scope.message.text ="Eliminado correctamente!";
          $scope.message.type="text-success";
          var index = $scope.empleados.indexOf($scope.emp);
          $scope.empleados.splice(index, 1);
        }
      });
    }
  };
});
