var app=angular.module("ServicesVerse",[]);
app.service("Api",function($http){
  	// var url = "http://localhost:5000";
	var url = "http://94.177.176.89:5000";
	return {
		getData: function(objeto,criterio){
			return $http.get(url+objeto);
		},
		deleteData: function(objeto,id){
			console.log("id funcion:"+id);
			return $http({method:'delete',url:url+objeto,data:{_id:id},headers:{"Content-Type": "application/json;charset=utf-8"}});
		},
		updateData: function(objeto,dato){
			return $http({method:'put',url:url+objeto,data:dato,headers:{"Content-Type": "application/json;charset=utf-8"}});
		},
    insertData: function(objeto,dato){
      return $http({method:'post',url:url+objeto,data:dato,headers:{"Content-Type": "application/json;charset=utf-8"}});
    }
	}
});
