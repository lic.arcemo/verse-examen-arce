var express = require("express");
var router = express.Router();
var bodyParser  = require("body-parser");
var path = require('path');
var app = express();

var apiEmpleado = require("./api/apiEmpleado"); // Métodos GET, POST, PUT, DELETE de Empleados
apiEmpleado.get(router);
app.use(bodyParser.json()); //Parsea application/json


app.use(express.static(path.join(__dirname, 'public')));
app.use('/src', express.static(__dirname + '/node_modules/'));
app.get('/', function(req, res) {
    res.sendFile('public/index.html');
});
app.use(router);
app.listen(5000, function(){
  console.log('Api Rest is running... Verse Consulting');
});
