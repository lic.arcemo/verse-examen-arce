var conn = require('../models/connection');
var mysql = require('mysql')
connection = mysql.createConnection(conn);

var empleado = {};

empleado.getEmpleados = function (callback){

  if (connection) {
    connection.query('SELECT * FROM Empleado', function(error, rows) {
      if(error){
        throw error;
      }
      else {
      callback(null, rows);
      }
    });
  }
  else {

  }
}

//Añadir sucursal
empleado.new = function(data,res) {
  if (connection){
    connection.query('INSERT INTO Empleado SET ?',data, function(error, result){
      if(error){
        console.log(error);
    		res.status(500).send(error);
      }
      else{
    			//devolvemos el id del usuario insertado
          data.idEmpleado=result.insertId;
          data.mensaje = "ok";
    			res.send(data);
    	}
    });
  }
};

empleado.delete = function(data,res){
  console.log(data);
  if(connection){
    connection.query('DELETE FROM Empleado WHERE idEmpleado = '+connection.escape(data._id.idEmpleado), function(error, result){
      if(error){
        console.log(error);
    		res.status(500).send(error);
      }
      else{
        console.log("eliminó: "+data._id.idEmpleado);
    			res.send({mensaje:"ok"});
    	}
    });
  }
}

empleado.update = function(data,res){
  if(connection){
    connection.query('UPDATE Empleado SET RFC = ' + connection.escape(data.RFC)  +', PrincipalName='+ connection.escape(data.PrincipalName)+', BusinessName='+ connection.escape(data.BusinessName)+', Phone='+ connection.escape(data.Phone)+', Email='+ connection.escape(data.Email)+', status='+ connection.escape(data.status)+' WHERE idEmpleado = ' + data.idEmpleado, function(error, result){
      if(error){
        console.log(error);
    		res.status(500).send(error);
      }
      else{
        data.mensaje='ok';
    			res.send(data);
    	}
    });
  }
}
module.exports = empleado;
